8/11/16 
	Merged scripts together utilizing the function command. 
	Improved wording on echo statements to avoid confusion and accidental nuking of disk.
	added tree dependency checker and install aid. 
	added support for all image types rather than just ones made by tj-dd-helper.sh