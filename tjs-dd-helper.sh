#!/bin/bash

##TABLE OF CONTENTS##
#FUNC_SECTION_00 - Jumps to functions section
#	ClONE FUNCTION - CLONE01
#	BACKUP FUNCTION - BACKUP01
#	RESTORE FUNCTION - RESTORE00
#		-IMAGE TYPE CHECKER - RESTORE01
#	TREE INSTALL FUNCTION - TREE01
#STARTUP_MENU_00 - Jumps to main menu section of code
###Update 6/11/18 added functions section and merged seperate scripts into function section###



##################################FUNCTION SECTION######################################## (FUNC_SECTION_OO)
#######CLONE FUNCTION######## (CLONE01)
function func_clone () {
		#TJ's dd clone helper

echo "**************CLONE FUNCTION**************"
read -rsn1 -p"Press any key to continue";echo
##list block devices
lsblk

#select first device

echo "select first device"
	read var_disk_1
echo "you have selected $var_disk_1"

#selects second device

echo "select second device"
	read var_disk_2
echo "you have selected $var_disk_2"

#confirmation if yes is entered cloning begins if not displays abort message
echo "type yes to confirm cloning of $var_disk_1 over $var_disk_2"
	read var_confirm
	if [[ $var_confirm == yes ]]; then
		dd if="/dev/$var_disk_1" of="/dev/$var_disk_2" conv=sync,noerror bs=64K status=progress
	else
		echo "operation aborted"
	fi
echo "operation complete"

read -rsn1 -p"Press any key to continue";echo

}

### Checks extension type and uses appropriate DD command ### (RESTORE01)
function func_extension_check () {
	if [[ $var_image =~ \.img.gz ]]; then 
		sudo gzip -cd $var_image | sudo dd of=/dev/$var_disk status=progress
	else
		sudo dd if=$var_image of=/dev/$var_disk bs=16K status=progress
	fi
} 

#####RESTORE FUNCTION########(RESTORE00)
function func_restore () {
echo "**************RESTORE FUNCTION**************"
read -rsn1 -p"Press any key to continue";echo

# Displays the home directory using the tree command if user inputs yes may try to replace this with curses or dialog later. 
# Possibly with my own command line pseudo(heh get it?) file manager
	echo "Show home directory?"
		read var_show_home
			if [[ $var_show_home == yes ]]; then
				tree -L 4 /home/
			else
				echo "..."
			fi
#User types out full path for gzipped disk image which sets var_image to said input. Again, would like to replace this later.
        echo "Select recovery image"
		read var_image	
        echo "you have selected $var_image"
# Displays block devices and prompts user for block device name. User's input sets var_disk which is then confirmed.

		lsblk
        echo "select disk to clone recovery image to"
                read var_disk
        echo "you have selected $var_disk"
        	read -rsn1 -p"Press any key to continue";echo
# Confirms the path and image of the backup and the path of the block device, prompts user for input expecting yes to continue
# any other input will stop the script and display the cancelled message
        echo "Are you sure you wish to clone $var_image to $var_disk?"
        echo "type yes to continue" 
                read var_confirmation
        if [[ $var_confirmation == yes ]]; then
#Same concept as before, unzips the gzip and writes the image back onto the block device. While saving on disk space, this method
#increases write time due to decompression while still requiring dd to write the compressed zeros back onto the block device
              func_extension_check
	else
		echo "operation cancelled"
	fi

}

#########BACKUP FUNCTION#########(BACKUP01)
function func_backup () {
echo "**************BACKUP HELPER**************" 
read -rsn1 -p"Press any key to continue";echo
lsblk #lists block devices
        echo "Select disk you wish to clone"
              #sets variable disk to user input
		 read var_disk 
        echo "you have selected $var_disk" 
        echo "select location to save backup to" 
                read var_backup_dir #sets variable backup directory to users input
        echo "You have selected $var_backup_dir as the location" 
        echo "select backup name"
                read var_backup_name #names backup
        echo "you have selected $var_backup_name as the name of the backup"
	 	read -rsn1 -p"Press any key to continue";echo #enter to continue
        echo "confirm cloning of $var_backup_name to $var_backup_dir" 
        echo "type yes to confirm backup"
                 #sets variable confirmation to user input can be anything but expects the word yes
		read var_confirm
       		#if user inputs yes, dd executes, clones the disk, and pipes it through gzip to compress the zeros thus saving 
		# storage space 

	if [[ $var_confirm == yes ]]; then 
               
                dd if="/dev/$var_disk" conv=sync,noerror bs=64K status=progress | gzip -c > $var_backup_dir/$var_backup_name.img.gz
       	#any other input aside from yes will cancel the operation and display cancelled message
	 else
                echo "operation cancelled" 
         fi
	#Otherwise, the script executes and displays completetion message w an enter to continue prompt
		echo "operation complete"
			read -rsn1 -p"Press any key to continue";echo
}

#Tree dependency function(TREE_01)

function func_tree () { 

#Checks if user would like help installing dependencies, and if user inputs yes installs tree for user
echo "I can help you install tree.(only works for Debian and Arch based distros)"
echo "Please select your distro 1)Debian 2)Arch 3)Quit"
			read distro_n
			case $distro_n in
				1) echo "You have selected Debian!"
					sudo apt-get -y install tree;;
				2) echo "You have selected Arch!"
					sudo pacman -S tree --noconfirm;;
				3) echo "Quitting"
					exit;;
				*) echo "invalid input";; 
			esac
read -rsn1 -p "Press any key to continue";echo
	bash ./tjs-dd-helper.sh
	
}


######################################END OF FUNCTIONS SECTION##########################################


######################################STARTUP MENU##############################################(STARTUP_MENU_00)
echo "****************Welcome to TJ's dd helper!************************"
read -rsn1 -p"Press any key to continue";echo

#Checks if tree is installed, and continues if it is. If it is not then it executes the tree install helper function (search for TREE01 to jump to it)
echo "checking for dependencies"
	if command -v tree > /dev/null 2>&1 ; then
		echo "tree found"
	else
		echo "tree not found. Executing install helper"
		func_tree
	fi
echo "Select a function!"
echo " 1)backup 2)clone 3)restore 4)quit "

read n
case $n in
	1) echo "You have selected backup!"
		read -rsn1 -p"Press any key to continue";echo
	 	func_backup;;
	2) echo "You have selected clone!"
		read -rsn1 -p"Press any key to continue";echo
		func_clone;;
	3) echo "You have selected restore!"
		read -rsn1 -p"Press any key to continue";echo
		 func_restore;;
	4) echo "Aborting operations"
		read -rsn1 -p"Press any key to continue";echo
		 exit;;
	*) echo "invalid option";;
esac

